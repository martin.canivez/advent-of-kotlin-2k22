import java.lang.reflect.InvocationTargetException
import java.time.LocalDate
import java.time.ZoneId

fun main() {
    println("Welcome to the advent of code !")
    for (day in 1..25) {
        try {
            val cname = "aoc2k22.days.D$day"
            val k = Class.forName(cname)
            val o = k.getConstructor().newInstance()
            for (part in 1..2) {
                val m = k.getMethod("runPart$part")
                println("Day $day part $part")
                println(m.invoke(o))
            }
            println("\n\n----------------------------------------------------\n\n")
        } catch (e: Exception) {
            errorHandling(day, e)
        }
    }
}

fun errorHandling(day: Int, e: Exception) {
    val message = when (e) {
        is ClassNotFoundException -> {
            val date = LocalDate.now(ZoneId.of("EST", ZoneId.SHORT_IDS))
            val unlockTime = LocalDate.parse("2022-12-${String.format("%02d", day)}")

            if (!date.isBefore(unlockTime)) "No class found even though challenge is available" else null
        }
        is NoSuchMethodException -> "No main method"
        is IllegalAccessException -> "Main can't be accessed"
        is InvocationTargetException -> e.targetException
        else -> e.message
    }

    message?.let { println("Issue for day $day : $it") }
}
