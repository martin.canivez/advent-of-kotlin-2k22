package aoc2k22.days

import aoc2k22.AdventDay
import java.io.BufferedReader
import java.math.BigInteger
import java.time.Duration
import java.time.Instant
import kotlin.properties.Delegates

private operator fun Instant.minus(lastTimeLogged: Instant) = Duration.between(lastTimeLogged, this)

class Y2021D21: AdventDay<Y2021D21.Game>(202121) {
    interface Game {
        val answer: BigInteger
    }

    data class GameP2(
        val players: List<PlayerP2>,
        val objective: Int,
        val nextPlayerIndex: Int
    ): Game {

        constructor(p1Start: Int, p2Start: Int, objective: Int, board: Board):
                this(listOf(PlayerP2(board, p1Start), PlayerP2(board, p2Start)), objective, 0)

        fun splitUniverse(): List<Pair<Int, Int>> =Array(3) { 3 downTo 1 }
            .fold(emptyList<Int>()) { acc, newProg ->
                if (acc.isEmpty())
                    newProg.map { it }
                else
                    newProg.flatMap { newEl -> acc.map { it + newEl }}
            }
            .groupingBy { it }
            .eachCount()
            .entries
            .map { it.toPair() }

        fun mapToPlayerWins(): List<BigInteger> {
            if (players.any { it.reached(objective) })
                return players.map { if (it.reached(objective)) BigInteger.ONE else BigInteger.ZERO }.also { universesResolved++ }
            else {
                return splitUniverse().map { newRoll ->
                    val newPlayers = players.mapIndexed { index, player ->
                        if (index == nextPlayerIndex) player.makeTurn(newRoll.first) else player
                    }
                    copy(
                        players = newPlayers,
                        nextPlayerIndex = (nextPlayerIndex + 1) % players.size
                    ).mapToPlayerWins()
                        .map { it * newRoll.second.toBigInteger() }
                }
                    .fold((Array(players.size) {BigInteger.ZERO}).asList()) { acc, child ->
                        acc.zip(child).map { it.first + it.second }
                    }
            }
        }

        override val answer: BigInteger by lazy {
            mapToPlayerWins().max()
        }

        companion object {
//            var amountOfUniverses: BigInteger by Delegates.observable(BigInteger.ONE) { _, _, new ->
//                println("Explode to $new universes")
//            }
            private var lastTimeLogged: Instant = Instant.now()

            var universesResolved: BigInteger by Delegates.observable(BigInteger.ZERO) { _, _, new ->
                if ((Instant.now() - lastTimeLogged) > Duration.ofSeconds(1)) {
                    println("Resolved $universesResolved universes")
                    lastTimeLogged = Instant.now()
                }
            }
        }
    }

    data class PlayerP2(val board: Board, val position: Int, val score: Int = 0) {
        fun reached(objective: Int) = score >= objective

        fun makeTurn(rolls: Int): PlayerP2 {
            val nextPosition = with(board) { getNextPosition(rolls) }
            return copy(position = nextPosition, score = score+nextPosition)
        }
    }

    class GameP1(p1Start: Int, p2Start: Int, val objective: Int): Game {
        val board = Board((1..10).toList())
        val players = listOf(Player(board, p1Start), Player(board, p2Start))
        val die = Die()

        fun perform() {
            val playerTurnIterable = generateSequence { players }.flatten()

            playerTurnIterable.map { it.makeTurn(die) }
                .takeWhile { players.none { it.score >= objective } }
                .last()
        }

        override val answer: BigInteger by lazy {
            perform()
            (players.minOf { it.score } * die.timesRolled).toBigInteger()
        }
    }

    data class Board(val availablePositions: List<Int>) {

        override fun toString() = "${this::class.simpleName}@${hashCode()}"

        fun Player.move(n: Int) {
            val currPosIndex = availablePositions.indexOf(position)
            val newPosIndex = (currPosIndex + n) % availablePositions.size
            position = availablePositions[newPosIndex]
        }

        fun PlayerP2.getNextPosition(n: Int): Int {
            val currPosIndex = availablePositions.indexOf(position)
            val newPosIndex = (currPosIndex + n) % availablePositions.size
            return availablePositions[newPosIndex]
        }
    }

    class Player(val board: Board, var position: Int) {
        fun makeTurn(die: Die) {
            val newMove = die.roll(3).sum()
            with(board) { move(newMove) }
            score += position
        }

        var score = 0; private set
    }

    class Die {
        var timesRolled = 0; private set
        private val numberGenerator = generateSequence { 1..100 }
            .flatten()
            .map { it.also { timesRolled++ } }
            .iterator()

        fun roll(amount: Int) = numberGenerator.asSequence().take(amount).toList()
    }

    override fun convertInputPart1(reader: BufferedReader): Game {
        val startPositions = reader.readLines().map { it.split(" ").last().toInt() }
        return GameP1(startPositions[0], startPositions[1], 1000)
    }

    override fun convertInputPart2(reader: BufferedReader): Game {
        val startPositions = reader.readLines().map { it.split(" ").last().toInt() }
        return GameP2(startPositions[0], startPositions[1], 21, Board((1..10).toList()))
    }

    override fun convertInput(reader: BufferedReader): InputData<Game> =
        throw IllegalStateException("Handled by p1 and p2 parsers")

    override fun part1(input: Game) = input.answer

    override fun part2(input: Game) = input.answer

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            Y2021D21().main()
        }
    }
}