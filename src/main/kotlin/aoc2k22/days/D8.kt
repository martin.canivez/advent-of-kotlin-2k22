package aoc2k22.days

import aoc2k22.AdventDay
import aoc2k22.toInputData
import com.google.common.collect.ImmutableTable
import com.google.common.collect.Table
import com.google.common.collect.Tables
import java.io.BufferedReader

class D8: AdventDay<D8.Input>(8) {
    class Input(private val table: Table<Int, Int, Tree>): Table<Int, Int, Tree> by table {
        fun isTreeVisible(row: Int, col: Int): Boolean {
            val theTree = table[row, col]!!
            return arrayOf(
                table.column(col).values.take(row),
                table.column(col).values.drop(row+1),
                table.row(row).values.take(col),
                table.row(row).values.drop(col+1)
            ).any { direction ->
                direction.none { otherTree -> otherTree.height >= theTree.height }
            }
        }

        fun scenicScore(row: Int, col: Int): Int {
            val theTree = table[row, col]!!
            return arrayOf(
                table.column(col).values.take(row).reversed(),
                table.column(col).values.drop(row+1),
                table.row(row).values.take(col).reversed(),
                table.row(row).values.drop(col+1)
            ).map { direction ->
                direction.takeWhile { otherTree -> otherTree.height < theTree.height }.size.let {
                    it + if (it == direction.size) 0 else 1 //Take into account whether vue is blocked or not
                }
            }.reduce(Int::times)
        }
    }

    data class Tree(val height: Int)

    override fun convertInput(reader: BufferedReader) =
        reader.lineSequence()
            .withIndex()
            .flatMap { (row, line) ->
                line.withIndex().map { (column, value) -> Tables.immutableCell(row, column, Tree(value.digitToInt())) } }
            .fold(ImmutableTable.builder<Int, Int, Tree>()) {acc, v ->
                acc.apply { put(v) }
            }
                .build()
                .let { Input(it) }
                .toInputData()

    override fun part1(input: D8.Input) =
        input.cellSet().count { input.isTreeVisible(it.rowKey, it.columnKey) }

    override fun part2(input: D8.Input) =
        input.cellSet().maxOf { input.scenicScore(it.rowKey, it.columnKey) }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            D8().main()
        }
    }
}