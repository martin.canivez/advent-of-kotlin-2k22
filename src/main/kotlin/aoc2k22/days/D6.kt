package aoc2k22.days

import aoc2k22.AdventDay
import aoc2k22.toInputData
import java.io.BufferedReader

typealias D6Input = Sequence<Char>

class D6 : AdventDay<D6Input>(6) {
    override fun convertInput(reader: BufferedReader) =
        reader.lineSequence()
            .flatMap { it.toCharArray().asSequence() }
            .toInputData()

    private fun <T> Sequence<List<T>>.endIndexOfFirstAllUnique() =
        this
            .withIndex()
            .first { it.value.toSet().size == it.value.size }
            .let { it.index + it.value.size }

    override fun part1(input: D6Input) =
        input.windowed(4)
            .endIndexOfFirstAllUnique()

    override fun part2(input: D6Input) =
        input.windowed(14)
            .endIndexOfFirstAllUnique()

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            D6().main()
        }
    }
}