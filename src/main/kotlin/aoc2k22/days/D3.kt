package aoc2k22.days

import aoc2k22.AdventDay
import aoc2k22.toInputData
import java.io.BufferedReader

private typealias D3Input = Sequence<D3.Rucksack>

class D3 : AdventDay<D3Input>(3) {
    class Rucksack(val content: String) {
        val left = content.take(content.length / 2)
        val right = content.takeLast(content.length / 2)
    }

    override fun convertInput(reader: BufferedReader) = reader.lineSequence()
        .map(::Rucksack)
        .toInputData()

    override fun part1(input: D3Input): Any =
        input
            .map { it.left.toSet() intersect it.right.toSet() }
            .map { it.first() }
            .sumOf { priorityMap[it]!! }

    override fun part2(input: D3Input): Any =
        input.chunked(3)
            .map { it.map { r -> r.content.toSet() } }
            .map { it.reduce { acc, r -> acc intersect r }.first() }
            .sumOf { priorityMap[it]!! }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            D3().main()
        }

        val priorityMap = (('a'..'z') + ('A'..'Z')).withIndex().associate { it.value to it.index + 1 }
    }
}
