package aoc2k22.days

import aoc2k22.AdventDay
import aoc2k22.toInputData
import java.io.BufferedReader

class D7: AdventDay<D7.Folder>(7) {
    sealed interface Node {
        val size: Int
    }

    sealed interface NodeInfo {
        companion object {
            fun fromString(str: String): NodeInfo = when {
                str.startsWith("dir") -> FolderInfo(str.split(" ").last())
                str.matches("^[0-9]+.*".toRegex()) -> with(str.split(" ")) { File(last(), first().toInt()) }
                else -> throw IllegalStateException(str)
            }
        }
    }

    open class Folder(val name: String, val parent: Folder?): Node {
        private val content = mutableMapOf<String, Node>()
        val nodes get() = content.values as Collection<Node>
        override val size get() = content.values.sumOf { it.size }

        fun computeFolderIfAbsent(name: String) =
            content.computeIfAbsent(name) {Folder(name, this)} as Folder

        fun addFiles(files: Iterable<File>) {
            content.putAll(files.map { it.name to it })
        }

        fun getRecursiveFolders(): Iterable<Folder> {
            return (sequenceOf(this) + nodes
                .filterIsInstance<Folder>()
                .flatMap { it.getRecursiveFolders() }).asIterable()
        }

        override fun toString(): String {
            return "Folder(name='$name')"
        }
    }
    data class FolderInfo(val name: String): NodeInfo
    data class File(val name: String, override val size: Int) : Node, NodeInfo

    sealed interface Command
    data class CdCommand(val folder: String): Command
    data class LsCommand(val content: Set<NodeInfo>): Command

    override fun convertInput(reader: BufferedReader) =
        reader.toCommandSequence()
            .toFolderStructure()
            .toInputData()

    override fun part1(input: Folder) =
        input.getRecursiveFolders()
            .filter { it.size <= 100000 }
            .sumOf { it.size }

    override fun part2(input: Folder): Int {
        val totalSpace = 70000000
        val neededUnusedSpace = 30000000
        val neededSave = -(totalSpace - neededUnusedSpace - input.size)

        return input.getRecursiveFolders()
            .filter { it.size >= neededSave }
            .minOf { it.size }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            D7().main()
        }

        fun Sequence<Command>.toFolderStructure(): Folder =
            fold(Folder("", null).let { it to it }) { (root, current), c ->
                root to when(c) {
                    is CdCommand -> when(c.folder) {
                        "/" -> root
                        ".." -> current.parent!!
                        else -> current.computeFolderIfAbsent(c.folder)
                    }
                    is LsCommand -> current.apply { addFiles(c.content.filterIsInstance<File>()) }
                }
            }
                .first

        fun BufferedReader.toCommandSequence() = sequence {
            var lsBuild = mutableSetOf<NodeInfo>()

            fun toYieldOnNewCommand(toYield: Command?) = sequenceOf(
                if (lsBuild.isNotEmpty()) LsCommand(lsBuild) else null,
                toYield
            ).filterNotNull()
                .also { lsBuild = mutableSetOf() }

            for (line in lineSequence()) {
                when {
                    line.startsWith("$ cd") -> yieldAll(toYieldOnNewCommand(CdCommand(line.split(" ").last())))
                    line == "$ ls" -> yieldAll(toYieldOnNewCommand(null))
                    else -> lsBuild.add(NodeInfo.fromString(line))
                }
            }
            yieldAll(toYieldOnNewCommand(null))
        }
    }
}