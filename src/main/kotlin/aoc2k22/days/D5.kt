package aoc2k22.days

import aoc2k22.AdventDay
import aoc2k22.toInputData
import java.io.BufferedReader

private typealias D5StackType = List<ArrayDeque<Char>>

class D5 : AdventDay<D5.Input>(5) {
    data class Input(val stacks: D5StackType, val moves: Sequence<Move>)

    data class Move private constructor(val amount: Int, val origin: Int, val destination: Int) {
        companion object {
            fun fromNatural(amount: Int, originNatural: Int, destinationNatural: Int) =
                Move(amount, originNatural - 1, destinationNatural - 1)
        }
    }

    override fun convertInput(reader: BufferedReader): InputData<Input> {
        reader.lines().iterator().let { iter ->
            val stackData = iter.asSequence()
                .takeWhile { it.isNotBlank() }
                .toMutableList()

            val stackAmount = stackData
                .removeLast()
                .split(Regex("\\s+"))
                .filter { it.isNotEmpty() }
                .maxOf { it.toInt() }

            val stackList = Array(stackAmount) { ArrayDeque<Char>() }

            stackData.forEach { e ->
                val col = e.chunked(4).map { it[1] }
                col.withIndex()
                    .filter { it.value.isLetter() }
                    .forEach { stackList[it.index].addFirst(it.value) }
            }

            val opReg = "move (\\d+) from (\\d+) to (\\d+)".toRegex()

            val remainingSeq = iter.asSequence()
                .map { opString ->
                    opReg.find(opString)!!.let {
                        Move.fromNatural(
                            it.groups[1]!!.value.toInt(),
                            it.groups[2]!!.value.toInt(),
                            it.groups[3]!!.value.toInt()
                        )
                    }
                }

            return Input(stackList.asList(), remainingSeq).toInputData()
        }
    }

    override fun part1(input: Input): String {
        fun D5StackType.perform(op: D5.Move) =
            map(::ArrayDeque)
                .apply {
                    repeat(op.amount) {
                        get(op.destination).add(get(op.origin).removeLast())
                    }
                }

        return input.moves
            .fold(input.stacks) { currentStack, op -> currentStack.perform(op) }
            .map { it.last() }
            .joinToString("")
    }

    override fun part2(input: Input): String {
        fun D5StackType.perform(op: D5.Move) =
            map(::ArrayDeque)
                .apply {
                    get(op.destination).addAll(Array(op.amount) { get(op.origin).removeLast() }.reversed())
                }

        return input.moves
            .fold(input.stacks) { currentStack, op -> currentStack.perform(op) }
            .map { it.last() }
            .joinToString("")
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            D5().main()
        }
    }
}
