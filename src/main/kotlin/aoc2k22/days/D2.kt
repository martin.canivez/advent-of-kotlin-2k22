package aoc2k22.days

import aoc2k22.AdventDay
import aoc2k22.toInputData
import java.io.BufferedReader

private typealias D2Input = Sequence<D2.Strat>

class D2 : AdventDay<D2Input>(2) {
    override fun convertInput(reader: BufferedReader) =
        reader
            .lineSequence()
            .map { l ->
                l.split(" ").let { chars -> Strat(adv = chars[0][0], own = chars[1][0]) }
            }
            .toInputData()

    override fun part1(input: D2Input): Any =
        input
            .map { s ->
                Battle(
                    adv = RPS.byAdvValue[s.adv]!!,
                    own = RPS.byOwnValue[s.own]!!
                )
            }
            .score()

    override fun part2(input: D2Input): Any =
        input
            .map { s ->
                Battle(
                    adv = RPS.byAdvValue[s.adv]!!,
                    outcome = Outcome.byChar[s.own]!!
                )
            }
            .score()

    private fun Sequence<Battle>.score() = map { it.score }.sum()

    enum class RPS(val adversaryVal: Char, val ownVal: Char, val score: Int) {
        ROCK('A', 'X', 1),
        PAPER('B', 'Y', 2),
        SCISSORS('C', 'Z', 3);

        fun outcomeAgainst(other: RPS): Outcome = when {
            this == other -> Outcome.DRAW
            (this.ordinal + 1) % 3 == other.ordinal -> Outcome.LOSE
            other.outcomeAgainst(this) == Outcome.LOSE -> Outcome.WIN
            else -> throw IllegalStateException()
        }

        companion object {
            val byAdvValue = RPS.values().associateBy { it.adversaryVal }
            val byOwnValue = RPS.values().associateBy { it.ownVal }
            val byOutcome = RPS.values()
                .flatMap { i -> RPS.values().map { j -> i to j } }
                .associate { (it.first to it.second.outcomeAgainst(it.first)) to it.second }
        }
    }

    enum class Outcome(val value: Char) {
        LOSE('X'),
        DRAW('Y'),
        WIN('Z');

        companion object {
            val byChar = Outcome.values().associateBy { it.value }
        }
    }

    data class Strat(val own: Char, val adv: Char)

    data class Battle(val own: RPS, val adv: RPS) {
        constructor(adv: RPS, outcome: Outcome) :
            this(RPS.byOutcome[adv to outcome]!!, adv)

        val score: Int
            get() = when (own.outcomeAgainst(adv)) {
                Outcome.LOSE -> 0
                Outcome.DRAW -> 3
                Outcome.WIN -> 6
            } + own.score
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            D2().main()
        }
    }
}
