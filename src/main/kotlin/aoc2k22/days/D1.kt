package aoc2k22.days

import aoc2k22.AdventDay
import aoc2k22.toInputData
import java.io.BufferedReader

private typealias D1Input = Sequence<D1.Elf>

class D1 : AdventDay<D1Input>(1) {
    data class Elf(val calories: List<Int>)

    override fun convertInput(reader: BufferedReader): SameInputData<D1Input> = sequence {
        val it = reader.lines().iterator()
        var current = arrayListOf<Int>()
        while (it.hasNext()) {
            val n = it.next()
            if (n.isNullOrEmpty()) {
                yield(Elf(current))
                current = arrayListOf()
            } else {
                current += n.toInt()
            }
        }
        yield(Elf(current))
    }.toInputData()

    override fun part1(input: D1Input): Any =
        input.maxOf { it.calories.sum() }

    override fun part2(input: D1Input): Any =
        input.map { it.calories.sum() }.sortedDescending().take(3).sum()

    companion object {
        @JvmStatic
        fun main(args: Array<String>) = D1().main()
    }
}
