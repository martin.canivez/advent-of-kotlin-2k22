package aoc2k22.days

import aoc2k22.*
import com.google.common.collect.Table
import com.google.common.collect.TreeBasedTable
import org.apache.commons.geometry.euclidean.twod.Vector2D
import java.io.BufferedReader
import kotlin.math.*

typealias D14CaveMap = Table<Int, Int, D14.TileType>
typealias D14Input = Sequence<D14.RockPath>

class D14: AdventDay<D14Input>(14) {
    data class RockPath(val from: Coordinate, val to: Coordinate) {
        fun rangeOf(member: Coordinate.() -> Int) = (member(from) to member(to)).let {
            if (it.first < it.second) it.first..it.second else it.second..it.first
        }
    }

    enum class TileType(val char: Char) {
        AIR('.'),
        ROCK('#'),
        SAND('o')
    }

    override fun convertInput(reader: BufferedReader): InputData<D14Input> = reader
        .lineSequence()
        .flatMap { it.split(" -> ").windowed(2).map { it[0] to it[1] } }
        .map { it.mapEach { it.split(",").let { Coordinate(it[0].toInt(), it[1].toInt()) } } }
        .map { RockPath(it.first, it.second) }
        .toInputData()

    private fun Sequence<RockPath>.buildMap(): D14CaveMap {
        val map = TreeBasedTable.create<Int, Int, TileType>()

        for (rockPath in this) {
            for (x in rockPath.rangeOf { x }) {
                for (y in rockPath.rangeOf { y }) {
                    map.put(y, x, TileType.ROCK)
                }
            }
        }

        val maxY = map.rowKeySet().max()+1
        val minX = map.columnKeySet().min()-1
        val maxX = map.columnKeySet().max()+1

        for (y in 0..maxY) {
            for (x in minX..maxX) {
                if (!map.contains(y, x))
                    map.put(y, x, TileType.AIR)
            }
        }

        return map
    }

    fun D14CaveMap.asString(): String {
        val digitsForCols = ceil(log10(columnKeySet().maxOf { abs(it)+1 }.toDouble())).toInt()
        val builder = StringBuilder()
        for (logDivisor in (digitsForCols-1) downTo 0) {
            builder.append(
                columnKeySet()
                    .map { if (it % 5 == 0) it else null }
                    .map { it?.let { (it / 10.toDouble().pow(logDivisor.toDouble())) % 10 } }
                    .map { it?.toInt()?.toString() }
                    .joinToString(separator = "", postfix = "\n") { it ?: " " })
        }

        builder.append("\n")

        for (row in rowMap()) {
            for (col in row.value.entries) {
                builder.append(col.value.char)
            }
            builder.append(" ${row.key}\n")
        }

        return builder.toString()
    }

    private fun Table<Int, Int, TileType>.putNewSand(isP1: Boolean = true): Unit? {
        val floor = this.rowKeySet().max()+1
        val startPosition = Coordinate(500, 0)
        var currSandCoordinate = startPosition

        while (true) {
            val nextValueDown = currSandCoordinate + Coordinate(0, 1)
            var nextTileDown = get(nextValueDown.y, nextValueDown.x)
            val nextValueLeft = currSandCoordinate + Coordinate(-1, 1)
            var nextTileLeft = get(nextValueLeft.y, nextValueLeft.x)
            val nextValueRight = currSandCoordinate + Coordinate(1, 1)
            var nextTileRight = get(nextValueRight.y, nextValueRight.x)

            if (!isP1) {
                nextTileDown = nextTileDown ?: if (nextValueDown.y >= floor) TileType.ROCK else TileType.AIR
                nextTileLeft = nextTileLeft ?: if (nextValueLeft.y >= floor) TileType.ROCK else TileType.AIR
                nextTileRight = nextTileRight ?: if (nextValueRight.y >= floor) TileType.ROCK else TileType.AIR
            }

            currSandCoordinate = when {
                nextTileDown == null -> return null
                nextTileDown == TileType.AIR -> nextValueDown
                nextTileLeft == TileType.AIR -> nextValueLeft
                nextTileRight == TileType.AIR -> nextValueRight
                currSandCoordinate == startPosition
                        && get(currSandCoordinate.y, currSandCoordinate.x) == TileType.SAND ->
                    return null
                else -> {
                    put(currSandCoordinate.y, currSandCoordinate.x, TileType.SAND)
                    return Unit
                }
            }
        }
    }

    override fun part1(input: D14Input): Any {
        val caveMap = input.buildMap()

        while (true)
            caveMap.putNewSand() ?: break

        return caveMap.cellSet().count { it.value == TileType.SAND }
    }

    override fun part2(input: D14Input): Any {
        val caveMap = input.buildMap()

        while (true)
            caveMap.putNewSand(false) ?: break

        return caveMap.cellSet().count { it.value == TileType.SAND }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            D14().main()
        }
    }
}
