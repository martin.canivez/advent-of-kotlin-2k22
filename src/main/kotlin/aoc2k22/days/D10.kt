package aoc2k22.days

import aoc2k22.AdventDay
import aoc2k22.mapValue
import aoc2k22.toInputData
import com.google.common.collect.ImmutableTable
import com.google.common.collect.Tables
import java.io.BufferedReader
import kotlin.math.abs

typealias D10Input = Sequence<D10.Operation>

class D10: AdventDay<D10Input>(10) {
    data class Computer(val x: Int = 1)

    sealed interface Operation {
        val cyclesToExecute: Int

        fun applyOn(c: Computer): Computer

        class Noop : Operation {
            override val cyclesToExecute = 1
            override fun applyOn(c: Computer) = c
        }

        class AddX(private val toAdd: Int): Operation {
            override val cyclesToExecute = 2
            override fun applyOn(c: Computer) = c.copy(x = c.x + toAdd)
        }
    }

    override fun convertInput(reader: BufferedReader): InputData<D10Input> =
        reader.lineSequence()
            .map { it.split(" ") }
            .map {
                when (it[0]) {
                    "addx" -> Operation.AddX(it[1].toInt())
                    "noop" -> Operation.Noop()
                    else -> throw IllegalStateException()
                }
            }
            .toInputData()

    override fun part1(input: D10Input): Int = input
        .toComputerSequence()
        .withIndex()
        .toDuringCycle()
        .filter { (index) -> index in consideredIndexes }
        .sumOf { it.index * it.value.x }

    override fun part2(input: D10Input) = input
        .toComputerSequence()
        .toScreenPixels(40)
        .fold(ImmutableTable.builder(), ImmutableTable.Builder<Int, Int, Char>::put)
        .build()
        .rowMap().values
        .joinToString("\n") { it.values.joinToString("") }

    private fun Sequence<Computer>.toScreenPixels(screenWidth: Int) = this
        .withIndex()
        .windowed(screenWidth, screenWidth)
        .flatMapIndexed { row, computerStatesList ->
            computerStatesList.map {
                    (col, computer) -> Tables.immutableCell(row, col % screenWidth, computer)
            }
        }
        .map { it.mapValue { _, column, computer ->
            if (abs(computer.x - column) <= 1) '#' else ' '
        } }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            D10().main()
        }

        fun D10Input.toComputerSequence() = this
            .runningFold(IndexedValue(0, Computer())) { (currentCycle, computer), operation ->
                IndexedValue(currentCycle + operation.cyclesToExecute, operation.applyOn(computer))
            }
            .flatMap(object : ((IndexedValue<Computer>) -> Iterable<Computer>) {
                private var lastPair: IndexedValue<Computer>? = null

                override fun invoke(p: IndexedValue<Computer>): List<Computer> {
                    val intermediateSteps = lastPair?.let { (lastCounter, lastComputer) ->
                        ((lastCounter+1) until p.index).map { lastComputer }
                    } ?: emptyList()
                    lastPair = p
                    return intermediateSteps + p.value
                }
            })

        fun <T> Sequence<IndexedValue<T>>.toDuringCycle() = this.map { it.copy(index = it.index+1) }

        val consideredIndexes = arrayOf(20, 60, 100, 140, 180, 220)
    }
}