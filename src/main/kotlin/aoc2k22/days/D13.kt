package aoc2k22.days

import aoc2k22.AdventDay
import aoc2k22.toInputData
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.NumericNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import java.io.BufferedReader

typealias D13Input = Sequence<D13.PacketData>

class D13: AdventDay<D13Input>(13) {
    sealed class PacketData: Comparable<PacketData> {
        override fun compareTo(other: PacketData): Int {
            return if (this is ListData && other is IntData)
                compareValues(this, other.toListData())
            else if (this is IntData && other is ListData)
                compareValues(this.toListData(), other)
            else if (this is IntData && other is IntData)
                compareValuesBy(this, other) { it.v }
            else if (this is ListData && other is ListData) {
                this.list.zip(other.list)
                    .map { compareValues(it.first, it.second) }
                    .firstOrNull { it != 0 }
                    ?: compareValuesBy(this, other) { it.list.size }
            } else
                throw IllegalStateException()
        }
    }

    data class IntData(val v: Int): PacketData() {
        fun toListData() = ListData(listOf(this))

        override fun toString() = v.toString()
    }

    data class ListData(val list: List<PacketData>): PacketData() {
        constructor(element: PacketData): this(listOf(element))
        override fun toString() = list.joinToString(prefix = "[", postfix = "]")
    }

    override fun convertInput(reader: BufferedReader) = reader
        .lineSequence()
        .filter { it.isNotEmpty() }
        .map { it.toPacketData() }
        .toInputData()

    override fun part1(input: D13Input) = input
        .windowed(2, 2)
        .map { it[0] to it[1] }
        .withIndex()
        .filter { it.value.first <= it.value.second }
        .sumOf { it.index+1 }

    override fun part2(input: D13Input): Int {
        val decoderPackets = arrayOf(
            ListData(ListData(IntData(2))),
            ListData(ListData(IntData(6)))
        )
        return (input + decoderPackets)
            .sorted()
            .withIndex()
            .filter { it.value in decoderPackets }
            .fold(1) { acc, packet -> acc*(packet.index+1)}
    }

    companion object {
        private val jsonMapper = jacksonObjectMapper()
        private fun String.toPacketData(): PacketData {

            val fromJson = jsonMapper.readTree(this)
            fromJson.asInt()
            return when (fromJson) {
                is NumericNode -> IntData(fromJson.asInt())
                is ArrayNode -> ListData(fromJson.elements().asSequence().map { it.toString().toPacketData() }.toList())
                else -> throw IllegalStateException()
            }
        }

        @JvmStatic
        fun main(args: Array<String>) {
            D13().main()
        }
    }
}