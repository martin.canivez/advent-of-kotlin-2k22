package aoc2k22.days

import aoc2k22.AdventDay
import org.apache.commons.numbers.core.ArithmeticUtils
import java.io.BufferedReader

class D11: AdventDay<D11.Input>(11) {

    data class Input(val myself: Myself, val monkeys: List<Monkey>)
    data class Item(var worryLevel: Long)

    class Monkey(
        itemsHeld: List<Item>,
        private val worryOp: (Long) -> Long,
        val testModulo: Int,
        val trueMonkeyNb: Int,
        val falseMonkeyNb: Int,
        private val monkeyList: List<Monkey>,
        private val observer: Myself
    ) {
        private val _itemsHeld = itemsHeld.toMutableList()
        var inspectionNb = 0; private set

        fun inspectAllItems() {
            for (i in _itemsHeld.toList()) {
                i.inspectAndThrow()
            }
        }

        private fun Item.inspectAndThrow() {
            inspectionNb++
            val worryLevel = observer.handleStress(worryOp(this.worryLevel))
            this.worryLevel = worryLevel
            val destMonkeyNb = if (worryLevel % testModulo == 0L) trueMonkeyNb else falseMonkeyNb
            _itemsHeld.remove(this)
            monkeyList[destMonkeyNb].receive(this)
        }

        private fun receive(i: Item) = _itemsHeld.add(i)
    }

    fun interface Myself {
        fun handleStress(i: Long): Long
    }

    override fun convertInputPart1(reader: BufferedReader): Input {
        val myself = Myself { i -> i / 3 }
        val monkeys = reader
            .lineSequence()
            .windowed(7, 7, true)
            .fold(mutableListOf<Monkey>()) { list, monkeyString ->
                list.apply {
                    add(buildMonkeyFromString(monkeyString, this, myself))
                }
            }

        return Input(myself, monkeys)
    }

    override fun convertInputPart2(reader: BufferedReader): Input {
        val myself = object : Myself {
            var monkeyLcm = 0L
            override fun handleStress(i: Long) = i % monkeyLcm
        }

        val monkeys = reader
            .lineSequence()
            .windowed(7, 7, true)
            .fold(mutableListOf<Monkey>()) { list, monkeyString ->
                list.apply {
                    add(buildMonkeyFromString(monkeyString, this, myself))
                }
            }

        myself.monkeyLcm = monkeys.fold(1) { acc, monkey -> ArithmeticUtils.lcm(acc, monkey.testModulo.toLong()) }

        return Input(myself, monkeys)
    }

    override fun convertInput(reader: BufferedReader): InputData<Input> {
        throw IllegalStateException("Handled differently for P1 and P2")
    }


    fun Input.performRound() = this
        .monkeys
        .forEach { m ->
            m.inspectAllItems()
        }

    override fun part1(input: Input): Int {
        repeat(20) {
            input.performRound()
        }

        return input.monkeys
            .asSequence()
            .sortedByDescending { it.inspectionNb }
            .take(2)
            .fold(1) { acc, monkey -> acc * monkey.inspectionNb }
    }

    override fun part2(input: Input): Long {
        repeat(10000) {
            input.performRound()
        }

        return input.monkeys
            .asSequence()
            .sortedByDescending { it.inspectionNb }
            .take(2)
            .fold(1L) { acc, monkey -> acc * monkey.inspectionNb }
    }

    companion object {
        fun buildMonkeyFromString(s: List<String>, monkeyList: List<Monkey>, observer: Myself): Monkey {
            val itemList = s[1]
                .split(":")[1] //Remove useless text
                .split(",") //Split per item
                .map { Item(it.trim().toLong()) }

            val operationOperands = s[2]
                .split("=")[1].trim() //Remove useless text
                .split(" ")

            val i1 = { old: Long -> toOpInput(operationOperands[0], old) }
            val theOperation = toOpOperation(operationOperands[1])
            val i2 = { old: Long -> toOpInput(operationOperands[2], old) }
            val op = { old: Long -> theOperation(i1(old), i2(old)) }

            val divisor = s[3].split(" ").last().toInt()

            val trueMonkey = s[4].split(" ").last().toInt()
            val falseMonkey = s[5].split(" ").last().toInt()

            return Monkey(itemList, op, divisor, trueMonkey, falseMonkey, monkeyList, observer)
        }

        fun toOpInput(s: String, oldWorry: Long): Long = when (s) {
            "old" -> oldWorry
            else -> s.toLong()
        }

        fun toOpOperation(s: String): (Long, Long) -> Long = when (s) {
            "+" -> Long::plus
            "-" -> Long::minus
            "*" -> Long::times
            "/" -> Long::div
            else -> throw IllegalStateException()
        }

        @JvmStatic
        fun main(args: Array<String>) {
            D11().main()
        }
    }
}