package aoc2k22.days

import aoc2k22.*
import org.apache.commons.geometry.euclidean.twod.RegionBSPTree2D
import org.apache.commons.geometry.euclidean.twod.Vector2D
import org.apache.commons.geometry.euclidean.twod.path.LinePath
import org.apache.commons.numbers.core.Precision
import java.io.BufferedReader

class D15: AdventDay<D15.Input>(15) {
    data class Beacon(val position: Coordinate)

    data class Sensor(val position: Coordinate, val distanceCovered: Int) {
        constructor(position: Coordinate, beacon: Beacon):
                this(position, (position.vector.euclideanDistance(beacon.position.vector)).toInt())
    }

    data class Input(val sensors: List<Sensor>, val beacons: List<Beacon>) {
        val areaToConsider get(): Pair<Coordinate, Coordinate> {
            val minX = sensors.minOf { it.position.x - it.distanceCovered }
            val maxX = sensors.maxOf { it.position.x + it.distanceCovered }
            val minY = sensors.minOf { it.position.y - it.distanceCovered }
            val maxY = sensors.maxOf { it.position.y + it.distanceCovered }

            return Coordinate(minX, minY) to Coordinate(maxX, maxY)
        }
    }

    override fun convertInput(reader: BufferedReader): InputData<Input> {
        return reader
            .lineSequence()
            .map {
                val splitted = it.split("[ ,:=]+".toRegex())
                val sxStr = splitted[3]
                val syStr = splitted[5]
                val bxStr = splitted[11]
                val byStr = splitted[13]

                Beacon(Coordinate(bxStr.toInt(), byStr.toInt())).let {
                    Sensor(
                        Coordinate(sxStr.toInt(), syStr.toInt()),
                        it
                    ) to it
                }
            }
            .fold(mutableListOf<Sensor>() to mutableListOf<Beacon>()) { acc, value ->
                acc.also {
                    it.first.add(value.first)
                    if (it.second.find { it.position == value.second.position } == null) it.second.add(value.second)
                }
            }
            .let { Input(it.first, it.second) }
            .toInputData()
    }

    override fun part1(input: Input): Int {
        val y = 2000000
        return ((input.areaToConsider.first.x)..(input.areaToConsider.second.x)).count { x ->
            val coord = Coordinate(x, y)

            when {
                input.beacons.any { it.position == coord } ->
                    false
                input.sensors.any { it.position.vector.euclideanDistance(coord.vector) <= it.distanceCovered } ->
                    true
                else ->
                    false
            }
        }
    }


    override fun part2(input: Input): Any {
        val p = Precision.doubleEquivalenceOfEpsilon(1e-6)
        val maxVal = 4000000.0;

        val regions = input.sensors
            .map {
                val lp = LinePath.builder(p)
                    .append((it.position - Coordinate(it.distanceCovered, 0)).vector)
                    .append((it.position - Coordinate(0, it.distanceCovered)).vector)
                    .append((it.position + Coordinate(it.distanceCovered, 0)).vector)
                    .append((it.position + Coordinate(0, it.distanceCovered)).vector)
                    .build(true)

                lp.toFiniteTree()
            }
            .fold(RegionBSPTree2D.empty()) { a, b -> a.apply { union(b) } }

        val candidateLinePath = LinePath.builder(p)
            .append(Vector2D.of(0.0, 0.0))
            .append(Vector2D.of(maxVal, 0.0))
            .append(Vector2D.of(maxVal, maxVal))
            .append(Vector2D.of(0.0, maxVal))
            .build(true)

        val candidateRegion = candidateLinePath
            .toInfiniteTree()

        val endRegion = regions.copy().apply { union(candidateRegion) }
        val boundary = endRegion
            .boundaryPaths
            .single()

        return Coordinate(boundary.toFiniteTree().centroid).let { it.x * maxVal.toLong() + it.y }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            D15().main()
        }
    }
}