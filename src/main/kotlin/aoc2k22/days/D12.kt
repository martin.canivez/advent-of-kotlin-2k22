package aoc2k22.days

import aoc2k22.AdventDay
import aoc2k22.Coordinate
import aoc2k22.Direction
import aoc2k22.*
import com.google.common.collect.ImmutableTable
import com.google.common.collect.Table
import com.google.common.collect.Tables
import com.google.common.collect.TreeBasedTable
import java.io.BufferedReader

class D12: AdventDay<D12.Input>(12) {
    data class Input(val start: Coordinate, val end: Coordinate, val map: Table<Int, Int, Int>) {

        fun buildTrajectoryToEnd(): ObjectiveItinerary {
            val trajectoryToEnd: Table<Int, Int, Coordinate> = TreeBasedTable.create()
            val coordsToCheck = mutableListOf(end)

            while(true) {
                val currCoord = coordsToCheck.removeFirstOrNull() ?: break

                val currAlt = map[currCoord]
                    ?: continue //Skip if we have no elevation on current cell

                for (d in Direction.values()) {
                    val directionCoord = currCoord + d.asCoordinate
                    if (trajectoryToEnd.contains(directionCoord) || directionCoord == end)
                        continue //As BFS, we know a path already found can't be longer than current one; skipping

                    val directionAlt = map[directionCoord]
                        ?: continue //Skip if we have no elevation for destination cell

                    if (currAlt - directionAlt <= 1) { //If current pos (we're climbing down) is not more than 1 higher
                        trajectoryToEnd.put(directionCoord.x, directionCoord.y, currCoord)
                        coordsToCheck.add(directionCoord)
                    }
                }
            }

            return ObjectiveItinerary(end, trajectoryToEnd)
        }
    }

    override fun convertInput(reader: BufferedReader): InputData<Input> {
        lateinit var start: Coordinate
        lateinit var end: Coordinate
        val map = reader.lineSequence()
            .flatMapIndexed { row, line ->
                line.mapIndexed { col, char-> Tables.immutableCell(
                    row,
                    col,
                    when(char) {
                        'S' -> 0.also { start = Coordinate(row, col) }
                        'E' -> 25.also { end = Coordinate(row, col) }
                        else -> char.code - 'a'.code
                    })
                }
            }
            .fold(ImmutableTable.builder<Int, Int, Int>()) { acc, cell -> acc.put(cell) }
            .build()

        return Input(start, end, map).toInputData()
    }

    data class ObjectiveItinerary(val objective: Coordinate, val towardsMap: Table<Int, Int, Coordinate>) {
        fun stepsFrom(origin: Coordinate): Int {
            var currentCoord = origin
            var stepsCount = 0

            while (currentCoord != objective) {
                towardsMap[currentCoord.x, currentCoord.y].let {
                    if (it == null)
                        return Int.MAX_VALUE
                    currentCoord = it
                }
                stepsCount++
            }

            return stepsCount
        }
    }

    override fun part1(input: Input) = input.buildTrajectoryToEnd().stepsFrom(input.start)

    override fun part2(input: Input): Any {
        val itineraryMap = input.buildTrajectoryToEnd()

        return input.map.cellSet().filter { it.value == 0 }
            .map { Coordinate(it.rowKey, it.columnKey) }
            .minOf { itineraryMap.stepsFrom(it) }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            D12().main()
        }
    }
}