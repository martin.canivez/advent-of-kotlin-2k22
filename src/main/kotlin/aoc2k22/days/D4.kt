package aoc2k22.days

import aoc2k22.AdventDay
import aoc2k22.toInputData
import java.io.BufferedReader

private typealias D4Input = Sequence<Pair<IntRange, IntRange>>

class D4 : AdventDay<D4Input>(4) {
    override fun convertInput(reader: BufferedReader) = reader.lineSequence()
        .map { elfTask ->
            elfTask.split(",")
                .map { it.split("-").map(String::toInt) }
        }
        .map { it.map { elf -> elf[0]..elf[1] } }
        .map { it[0] to it[1] }
        .toInputData()


    override fun part1(input: D4Input): Any = input
        .filter { it.toList().any { e -> e.toSet() == (it.first.toSet() intersect it.second.toSet()) } }
        .count()

    override fun part2(input: D4Input): Any = input
        .filter { pair ->
            pair.toList()
                .map(IntRange::toSet)
                .reduce(Iterable<Int>::intersect)
                .isNotEmpty()
        }
        .count()

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            D4().main()
        }
    }
}
