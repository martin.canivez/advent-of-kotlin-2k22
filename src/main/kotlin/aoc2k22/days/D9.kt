package aoc2k22.days

import aoc2k22.*
import org.apache.commons.geometry.euclidean.twod.Vector2D
import java.io.BufferedReader

typealias D9Input = Sequence<Pair<Direction, Int>>

class D9: AdventDay<D9Input>(9) {

    class RopeLink {
        var position = Coordinate(0, 0)

        var next: RopeLink? = null
            set(value) { if (field == null) field = value }

        var previous: RopeLink? = null
            set(value) { if (field == null) field = value }

        fun move(d: Direction) {
            if (previous != null) throw IllegalStateException("Is not the head")

            position += d.asCoordinate
            next?.followPrevious()
        }

        private fun followPrevious(): Unit = previous.let { previous ->
            if (previous == null) throw IllegalStateException("Is the head")

            val distanceTailToHead = (previous.position - this.position)

            if (distanceTailToHead.vector.normSq() > 2) {
                this.position += distanceTailToHead.closestCardinalMove()
            }
            next?.followPrevious()
        }
    }

    class Rope(linkNb: Int) {
        val head = RopeLink()
        val tail: RopeLink

        init {
            var lastMade = head
            repeat(linkNb-1) {
                val newLink = RopeLink()
                newLink.previous = lastMade
                lastMade.next = newLink
                lastMade = newLink
            }
            tail = lastMade
        }

        fun move(d: Direction) {
            head.move(d)
        }
    }

    fun D9Input.appliedTo(r: D9.Rope) =
        flatMap { m -> List(m.second) { m.first } }
            .runningFold(r) { acc, direction -> acc.apply { move(direction) } }

    fun Sequence<D9.Rope>.foldToVisitedCells() =
        fold(mutableSetOf<Coordinate>()) { acc, rope ->
            acc.apply { add(rope.tail.position) }
        }

    override fun convertInput(reader: BufferedReader): InputData<D9Input> =
        reader.lineSequence()
            .map { it.split(" ") }
            .map { Direction.fromLetter(it[0].first()) to it[1].toInt() }
            .toInputData()

    override fun part1(input: D9Input) =
        input.appliedTo(Rope(2))
            .foldToVisitedCells()
            .size

    override fun part2(input: D9Input) =
        input.appliedTo(Rope(10))
            .foldToVisitedCells()
            .size

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            D9().main()
        }
    }
}