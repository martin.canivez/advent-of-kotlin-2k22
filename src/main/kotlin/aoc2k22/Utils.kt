package aoc2k22

import com.google.common.collect.Table
import com.google.common.collect.Tables
import org.apache.commons.geometry.euclidean.twod.BoundarySource2D
import org.apache.commons.geometry.euclidean.twod.Vector2D
import org.apache.commons.geometry.euclidean.twod.rotation.Rotation2D
import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.math.abs
import kotlin.math.roundToInt

fun <T> T.toInputData(): AdventDay.SameInputData<T> = AdventDay.SameInputData(this)
fun <T> Pair<T, T>.toInputData(): AdventDay.InputData<T> = AdventDay.InputData(first, second)

fun <T, R> Pair<T, T>.mapEach(block: (T) -> R) = block(first) to block(second)

operator fun Vector2D.plus(other: Vector2D) = this.add(other)!!
operator fun Vector2D.minus(other: Vector2D) = this.subtract(other)!!
operator fun Vector2D.div(other: Double) = this.multiply(1.0/other)!!
fun Vector2D.euclideanDistance(other: Vector2D): Double {
    val xDiff = this.x - other.x
    val yDiff = this.y - other.y
    return abs(xDiff) + abs(yDiff)
}

infix fun Vector2D.rotateBy(r: Rotation2D): Vector2D = this.transform(r)

fun BoundarySource2D.toFiniteTree() = toSpecificTree(true)

private fun BoundarySource2D.toSpecificTree(finite: Boolean) = toTree().let {
    if (it.isFinite == finite) it else toTree().apply { complement() }
}

fun BoundarySource2D.toInfiniteTree() = toSpecificTree(false)

fun <R, C, V, O> Table.Cell<R, C, V>.mapValue(mapper: (R, C, V) -> O): Table.Cell<R, C, O> =
    Tables.immutableCell(rowKey, columnKey, mapper(rowKey, columnKey, value))

fun Coordinate.closestCardinalMove() =
    arrayOf(
        Direction.UP.asCoordinate,
        Direction.LEFT.asCoordinate,
        Direction.RIGHT.asCoordinate,
        Direction.DOWN.asCoordinate,
        Direction.UP.asCoordinate + Direction.LEFT.asCoordinate,
        Direction.UP.asCoordinate + Direction.RIGHT.asCoordinate,
        Direction.DOWN.asCoordinate + Direction.LEFT.asCoordinate,
        Direction.DOWN.asCoordinate + Direction.RIGHT.asCoordinate
    )
    .maxBy { it.vector.normalize().dot(this.vector) }

operator fun Int.times(toMultiply: Coordinate) = Coordinate(toMultiply.vector.multiply(this.toDouble())!!)

enum class Direction(x: Int, y: Int, val asArrow: Char) {
    UP(-1, 0, '^'),
    LEFT(0, -1, '<'),
    RIGHT(0, 1, '>'),
    DOWN(1, 0, 'v');

    val asVector: Vector2D = Vector2D.of(x.toDouble(), y.toDouble())
    val asCoordinate: Coordinate = Coordinate(x, y)

    companion object {
        fun fromVector2D(v: Vector2D) = Direction.values().maxBy { it.asVector.dot(v) }

        fun fromLetter(c: Char) = when (c) {
            'U' -> UP
            'L' -> LEFT
            'R' -> RIGHT
            'D' -> DOWN
            else -> throw IllegalStateException("$c is illegal")
        }
    }
}

data class Coordinate(val x: Int, val y: Int) {

    val vector = Vector2D.of(x.toDouble(), y.toDouble())

    constructor(c: Vector2D): this(c.x.roundToInt(), c.y.roundToInt())
    operator fun plus(other: Coordinate): Coordinate = Coordinate(this.vector + other.vector)
    operator fun minus(other: Coordinate): Coordinate = Coordinate(this.vector - other.vector)
}

operator fun <T> Table<Int, Int, T>.get(c: Coordinate) = get(c.x, c.y)
fun Table<Int, Int, *>.contains(c: Coordinate) = contains(c.x, c.y)

abstract class AdventDay<T>(val day: Int) {
    open class InputData<T> constructor(val p1: T, val p2: T)
    class SameInputData<T>(i: T) : InputData<T>(i, i)

    abstract fun convertInput(reader: BufferedReader): InputData<T>
    abstract fun part1(input: T): Any
    abstract fun part2(input: T): Any

    open fun convertInputPart1(reader: BufferedReader): T = convertInput(reader).p1
    open fun convertInputPart2(reader: BufferedReader): T = convertInput(reader).p2

    private fun getReader(day: Int): BufferedReader {
        val inputResource = this::class.java.getResource("/d$day.txt")
        return inputResource?.let { InputStreamReader(it.openStream()).buffered() }
            ?: throw IllegalStateException("No input file")
    }

    private fun readInputPart1() = convertInputPart1(getReader(day))
    fun runPart1() = part1(readInputPart1())

    private fun readInputPart2() = convertInputPart2(getReader(day))
    fun runPart2() = part2(readInputPart2())

    fun main() {
        println("Part 1:")
        println(runPart1())
        println("\nPart 2:")
        println(runPart2())
    }
}
