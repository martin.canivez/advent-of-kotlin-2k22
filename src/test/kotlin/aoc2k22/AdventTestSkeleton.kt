package aoc2k22

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringReader
import java.net.URL

sealed interface InputType {
    val reader: BufferedReader
}

class StringInput(private val str: String) : InputType {
    override val reader get() = StringReader(str.trimIndent()).buffered()
}

class UrlInput(private val url: URL?) : InputType {
    override val reader get() = url?.let { InputStreamReader(it.openStream()).buffered() }
        ?: throw IllegalStateException("No input file")
}

abstract class AdventTestSkeleton(private val day: Int) {
    open val input: InputType = resourceInput
    abstract val expectedP1: Any
    abstract val expectedP2: Any

    lateinit var challenge: AdventDay<Any>

    protected val resourceInput: InputType get() = UrlInput(this::class.java.getResource("/d$day.txt"))

    @BeforeEach
    fun setUp() {
        val cname = "aoc2k22.days.D$day"
        val k = Class.forName(cname)
        challenge = k.getConstructor().newInstance() as AdventDay<Any>
    }

    @Test
    fun testPart1() {
        val actual = challenge.part1(challenge.convertInputPart1(input.reader))
        assertEquals(expectedP1, actual)
    }

    @Test
    fun testPart2() {
        val actual = challenge.part2(challenge.convertInputPart2(input.reader))
        assertEquals(expectedP2, actual)
    }
}
