package aoc2k22.days

import aoc2k22.AdventTestSkeleton

class D4Test : AdventTestSkeleton(4) {
    override val expectedP1 = 524
    override val expectedP2 = 798
}
