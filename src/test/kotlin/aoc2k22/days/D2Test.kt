package aoc2k22.days

import aoc2k22.AdventTestSkeleton
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class D2Test : AdventTestSkeleton(2) {
    override val expectedP1 = 12645
    override val expectedP2 = 11756

    @Test
    fun validateBattleScore() {
        assertEquals(4, D2.Battle(D2.RPS.ROCK, D2.RPS.ROCK).score)
        assertEquals(1, D2.Battle(D2.RPS.ROCK, D2.RPS.PAPER).score)
        assertEquals(7, D2.Battle(D2.RPS.ROCK, D2.RPS.SCISSORS).score)

        assertEquals(8, D2.Battle(D2.RPS.PAPER, D2.RPS.ROCK).score)
        assertEquals(5, D2.Battle(D2.RPS.PAPER, D2.RPS.PAPER).score)
        assertEquals(2, D2.Battle(D2.RPS.PAPER, D2.RPS.SCISSORS).score)

        assertEquals(3, D2.Battle(D2.RPS.SCISSORS, D2.RPS.ROCK).score)
        assertEquals(9, D2.Battle(D2.RPS.SCISSORS, D2.RPS.PAPER).score)
        assertEquals(6, D2.Battle(D2.RPS.SCISSORS, D2.RPS.SCISSORS).score)
    }

    @Test
    fun validateOutcomeMapping() {
        assertEquals(D2.Outcome.WIN, D2.RPS.PAPER.outcomeAgainst(D2.RPS.ROCK))
        assertEquals(D2.Outcome.WIN, D2.RPS.SCISSORS.outcomeAgainst(D2.RPS.PAPER))
        assertEquals(D2.Outcome.WIN, D2.RPS.ROCK.outcomeAgainst(D2.RPS.SCISSORS))

        assertEquals(D2.Outcome.LOSE, D2.RPS.SCISSORS.outcomeAgainst(D2.RPS.ROCK))
        assertEquals(D2.Outcome.LOSE, D2.RPS.ROCK.outcomeAgainst(D2.RPS.PAPER))
        assertEquals(D2.Outcome.LOSE, D2.RPS.PAPER.outcomeAgainst(D2.RPS.SCISSORS))

        assertEquals(D2.Outcome.DRAW, D2.RPS.ROCK.outcomeAgainst(D2.RPS.ROCK))
        assertEquals(D2.Outcome.DRAW, D2.RPS.PAPER.outcomeAgainst(D2.RPS.PAPER))
        assertEquals(D2.Outcome.DRAW, D2.RPS.SCISSORS.outcomeAgainst(D2.RPS.SCISSORS))
    }
}
