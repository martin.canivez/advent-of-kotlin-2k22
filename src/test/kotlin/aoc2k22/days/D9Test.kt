package aoc2k22.days

import aoc2k22.AdventTestSkeleton
import aoc2k22.Direction
import aoc2k22.*
import org.apache.commons.geometry.euclidean.twod.Vector2D
import org.apache.commons.geometry.euclidean.twod.rotation.Rotation2D
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import kotlin.math.PI
import kotlin.test.assertEquals

class D9Test: AdventTestSkeleton(9) {
    override val input: InputType
        get() = StringInput("""
            R 5
            U 8
            L 8
            D 3
            R 17
            D 10
            L 25
            U 20
        """.trimIndent())
    override val expectedP1: Any
        get() = 88
    override val expectedP2: Any
        get() = 36

    @ParameterizedTest
    @EnumSource(Direction::class)
    fun checkMovementSameRow(d: Direction) {
        val r = D9.Rope(2)
        r.move(d)
        r.move(d)

        assertEquals(
            2 * d.asCoordinate,
            r.head.position
        )

        assertEquals(
            d.asCoordinate,
            r.tail.position
        )
    }

    @ParameterizedTest
    @EnumSource(Direction::class)
    fun checkMovementDiagonal(d: Direction) {
        val leftTurnDirection = Direction.fromVector2D(d.asVector rotateBy Rotation2D.of(PI/2))
        val r = D9.Rope(2)

        r.move(d)
        r.move(leftTurnDirection)
        r.move(d)

        assertEquals(
            2 * d.asCoordinate + leftTurnDirection.asCoordinate,
            r.head.position
        )

        assertEquals(
            d.asCoordinate + leftTurnDirection.asCoordinate,
            r.tail.position
        )
    }


    @ParameterizedTest
    @EnumSource(Direction::class)
    fun checkMovementLongRope(d: Direction) {
        val leftTurnDirection = Direction.fromVector2D(d.asVector rotateBy Rotation2D.of(PI/2))
        val r = D9.Rope(3)

        r.move(d)
        r.move(leftTurnDirection)
        r.move(d)
        r.move(d)

        assertEquals(
            3 * d.asCoordinate + leftTurnDirection.asCoordinate,
            r.head.position
        )

        assertEquals(
            2 * d.asCoordinate + leftTurnDirection.asCoordinate,
            r.head.next!!.position
        )

        assertEquals(
            d.asCoordinate + leftTurnDirection.asCoordinate,
            r.tail.position
        )
    }
}