package aoc2k22.days

import aoc2k22.AdventTestSkeleton
import aoc2k22.StringInput

class D15Test: AdventTestSkeleton(15) {

    override val expectedP1 = 5040643
    override val expectedP2 = 11016575214126L
}