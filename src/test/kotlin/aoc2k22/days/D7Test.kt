package aoc2k22.days

import aoc2k22.AdventTestSkeleton

class D7Test: AdventTestSkeleton(7) {
    override val expectedP1 = 1783610
    override val expectedP2  = 4370655
}