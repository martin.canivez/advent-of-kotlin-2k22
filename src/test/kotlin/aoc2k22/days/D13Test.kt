package aoc2k22.days

import aoc2k22.AdventTestSkeleton

class D13Test: AdventTestSkeleton(13) {
    override val expectedP1 = 6623
    override val expectedP2 = 23049
}