package aoc2k22.days

import aoc2k22.AdventTestSkeleton

class D12Test: AdventTestSkeleton(12) {
    override val expectedP1 = 350
    override val expectedP2 = 349
}