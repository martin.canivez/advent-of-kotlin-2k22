package aoc2k22.days

import aoc2k22.AdventTestSkeleton

class D3Test : AdventTestSkeleton(3) {
    override val expectedP1 = 8252
    override val expectedP2 = 2828
}
