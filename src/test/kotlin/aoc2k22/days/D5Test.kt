package aoc2k22.days

import aoc2k22.AdventTestSkeleton

class D5Test : AdventTestSkeleton(5) {
    override val expectedP1 = "WSFTMRHPP"
    override val expectedP2 = "GSLCMFBRP"
}
