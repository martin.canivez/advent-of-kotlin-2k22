package aoc2k22.days

import aoc2k22.AdventTestSkeleton

class D1Test : AdventTestSkeleton(1) {
    override val expectedP1 = 65912
    override val expectedP2 = 195625
}
