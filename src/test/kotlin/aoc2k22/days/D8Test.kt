package aoc2k22.days

import aoc2k22.AdventTestSkeleton
import aoc2k22.StringInput
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class D8Test: AdventTestSkeleton(8) {
    private val exampleInput = StringInput("""
            30373
            25512
            65332
            33549
            35390
        """.trimIndent())

    @Test
    fun testTreesVisible() {
        val input = challenge.convertInputPart1(exampleInput.reader) as D8.Input
        assertEquals(true, input.isTreeVisible(1, 1))
        assertEquals(true, input.isTreeVisible(1, 2))
        assertEquals(false, input.isTreeVisible(1, 3))
        assertEquals(true, input.isTreeVisible(2, 1))
        assertEquals(false, input.isTreeVisible(2, 2))
        assertEquals(true, input.isTreeVisible(3, 2))
        assertEquals(false, input.isTreeVisible(3, 1))
        assertEquals(false, input.isTreeVisible(3, 3))
    }

    @Test
    fun testScenicScore() {
        val input = challenge.convertInputPart1(exampleInput.reader) as D8.Input
        assertEquals(8, input.scenicScore(3, 2))
    }

    override val expectedP1 = 1705
    override val expectedP2 = 371200
}