package aoc2k22.days

import aoc2k22.AdventTestSkeleton

class D6Test: AdventTestSkeleton(6) {
    override val expectedP1 = 1658
    override val expectedP2 = 2260
}