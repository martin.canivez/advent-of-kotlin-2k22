package aoc2k22.days

import aoc2k22.AdventTestSkeleton
import aoc2k22.InputType
import aoc2k22.StringInput

class D11Test: AdventTestSkeleton(11) {
    override val input: InputType
        get() = StringInput("""
Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1
        """.trimIndent())

    override val expectedP1: Any
        get() = 10605
    override val expectedP2: Any
        get() = 2713310158L
}