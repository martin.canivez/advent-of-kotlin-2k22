package aoc2k22.days

import aoc2k22.AdventTestSkeleton
import aoc2k22.InputType
import aoc2k22.StringInput
import aoc2k22.days.D10.Companion.toComputerSequence
import aoc2k22.days.D10.Companion.toDuringCycle
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class D10Test: AdventTestSkeleton(10) {
    val aocInput: InputType
        get() = StringInput("""
            addx 15
            addx -11
            addx 6
            addx -3
            addx 5
            addx -1
            addx -8
            addx 13
            addx 4
            noop
            addx -1
            addx 5
            addx -1
            addx 5
            addx -1
            addx 5
            addx -1
            addx 5
            addx -1
            addx -35
            addx 1
            addx 24
            addx -19
            addx 1
            addx 16
            addx -11
            noop
            noop
            addx 21
            addx -15
            noop
            noop
            addx -3
            addx 9
            addx 1
            addx -3
            addx 8
            addx 1
            addx 5
            noop
            noop
            noop
            noop
            noop
            addx -36
            noop
            addx 1
            addx 7
            noop
            noop
            noop
            addx 2
            addx 6
            noop
            noop
            noop
            noop
            noop
            addx 1
            noop
            noop
            addx 7
            addx 1
            noop
            addx -13
            addx 13
            addx 7
            noop
            addx 1
            addx -33
            noop
            noop
            noop
            addx 2
            noop
            noop
            noop
            addx 8
            noop
            addx -1
            addx 2
            addx 1
            noop
            addx 17
            addx -9
            addx 1
            addx 1
            addx -3
            addx 11
            noop
            noop
            addx 1
            noop
            addx 1
            noop
            noop
            addx -13
            addx -19
            addx 1
            addx 3
            addx 26
            addx -30
            addx 12
            addx -1
            addx 3
            addx 1
            noop
            noop
            noop
            addx -9
            addx 18
            addx 1
            addx 2
            noop
            noop
            addx 9
            noop
            noop
            noop
            addx -1
            addx 2
            addx -37
            addx 1
            addx 3
            noop
            addx 15
            addx -21
            addx 22
            addx -6
            addx 1
            noop
            addx 2
            addx 1
            noop
            addx -10
            noop
            noop
            addx 20
            addx 1
            addx 2
            addx 2
            addx -6
            addx -11
            noop
            noop
            noop
        """.trimIndent())

    @Test
    fun confirmTestSequence() {
        val parsedInput = challenge.convertInputPart1(aocInput.reader) as D10Input
        val computerSequence = parsedInput.toComputerSequence()
            .withIndex()
            .toDuringCycle()

        assertEquals(13140,
            computerSequence.filter { it.index in D10.Companion.consideredIndexes }
                .sumOf { it.index * it.value.x })
    }

    override val expectedP1 = 13740
    override val expectedP2 = """
        #### #  # ###  ###  #### ####  ##  #    
           # #  # #  # #  # #    #    #  # #    
          #  #  # #  # #  # ###  ###  #    #    
         #   #  # ###  ###  #    #    #    #    
        #    #  # #    # #  #    #    #  # #    
        ####  ##  #    #  # #    ####  ##  #### 
    """.trimIndent()
}