# Advent of Kotlin 2k22

## Name
This repo is my personal code repository for the <https://adventofcode.com/2022>

## Installation
- Clone the repo
- Run the gradle install
- ???
- Profit

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Roadmap
Hopefully make at least the first 7 aoc2k22.days before the end of January 2023

## Authors and acknowledgment
Myself

## License
You may use my code however you please. The used tools/libraries are subject to their own license
